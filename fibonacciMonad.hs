{- Функция fib вычисляет n-ое число Фибоначчи при помощи монады State.
-}

import Control.Monad.State

fibStep :: State (Integer, Integer) ()
fibStep = do
	(a, b) <- get
	put (b, a + b)

execStateN :: Int -> State s a -> s -> s
execStateN n m = execState $ replicateM_ n m

fib :: Int -> Integer
fib n = fst $ execStateN n fibStep (0, 1)