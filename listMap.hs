{- Представитель MapLike для типа ListMap, определенный как список пар ключ-значение.
   Для каждого ключа должно храниться не больше одного значения.
   Функция insert заменяет старое значение новым, если ключ уже содержался в структуре.
-}

import Prelude hiding (lookup)
import qualified Data.List as L

class MapLike m where
    empty :: m k v
    lookup :: Ord k => k -> m k v -> Maybe v
    insert :: Ord k => k -> v -> m k v -> m k v
    delete :: Ord k => k -> m k v -> m k v
    fromList :: Ord k => [(k,v)] -> m k v
    fromList []         = empty
    fromList ((k,v):xs) = insert k v (fromList xs)

newtype ListMap k v = ListMap { getListMap :: [(k,v)] }
    deriving (Eq,Show)

instance MapLike ListMap where
	empty = ListMap []

	lookup _ (ListMap [])            = Nothing
	lookup key (ListMap ((k, v):xs)) = if key == k then Just v else lookup key (ListMap xs)

	delete _ (ListMap [])   = ListMap []
	delete key (ListMap xs) = ListMap (filter (\(k, v) -> k /= key) xs)

	insert key value (ListMap [])          = ListMap [(key, value)]
	insert key value (ListMap ((k, v):xs)) = if key == k then ListMap ((key, value):xs) else ListMap ((k,v):tail) where
		(ListMap tail) = insert key value (ListMap xs)