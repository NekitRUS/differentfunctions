{- Функция, которая нумерует вершины бинарного дерева данной формы,
   обходя их in-order (то есть, сначала левое поддерево, затем текущую вершину, затем правое поддерево).

   GHCi> numberTree (Leaf ())
   Leaf 1

   GHCi> numberTree (Fork (Leaf ()) () (Leaf ()))
   Fork (Leaf 1) 2 (Leaf 3)
-}

import Control.Monad.State

data Tree a = Leaf a | Fork (Tree a) a (Tree a) deriving Show

numberTree :: Tree () -> Tree Integer
numberTree tree = evalState (helper tree) 1 where
	helper (Leaf _) = do
		n <- get
		put (n + 1)
		return $ Leaf n
	helper (Fork left _ right) = do
		newLeft <- helper left
		n <- get
		put (n + 1)
		newRight <- helper right
		return (Fork newLeft n newRight)