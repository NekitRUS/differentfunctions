{- Программа просит пользователя ввести любую строку, а затем удаляет все файлы в текущей директории,
   в именах которых содержится эта строка, выдавая при этом соответствующие сообщения.

   Substring: hell
   Removing file: hello.world
   Removing file: linux_in_nutshell.pdf

   Если же пользователь ничего не ввёл, ничего не удаляетя и сообщается об этом:

   Substring: 
   Canceled
-}

module Main where

import System.IO (hFlush, stdout)
import System.Directory (getDirectoryContents, removeFile)
import Data.List (isInfixOf)

main :: IO ()
main = do
	putStr "Substring: "
	hFlush stdout
	filePrefix <- getLine
	if filePrefix == ""
		then putStrLn "Canceled"
		else do
			files <- getDirectoryContents "."
			filesToDel <- return (filter (isInfixOf filePrefix) files)
			mapM_ (\x -> removeFile x >> putStrLn ("Removing file: " ++ x)) filesToDel