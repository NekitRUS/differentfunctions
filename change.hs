{- Пусть есть список положительных достоинств монет coins, отсортированный по возрастанию.
   Функция change разбивает переданную ей положительную сумму денег на монеты достоинств из списка coins всеми возможными способами.

   Например, если coins = [2, 3, 7]:
   GHCi> change 7
   [[2,2,3],[2,3,2],[3,2,2],[7]]
-}

coins :: Num a => [a]
coins = [2, 3, 7]

change :: (Ord a, Num a) => a -> [[a]]
change 0 = [[]]
change sum = [ coin : changeArr | coin <- coins, coin <= sum, changeArr <- change (sum - coin) ]