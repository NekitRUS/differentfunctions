{- Плоскость разбита на квадратные ячейки. Стороны ячеек параллельны осям координат.
   Координаты углов ячейки с координатой (0,0) имеют неотрицательные координаты.
   Один из углов этой ячейки имеет координату (0,0).
   С ростом координат ячеек увеличиваются координаты точек внутри этих ячеек.

   Функция getCenter принимает координату ячейки и возвращает координату ее центра.
   Функция getCell принимает координату точки и возвращает номер ячейки в которой находится данная точка.
   В качестве первого аргумента обе эти функции принимают ширину ячейки.
-}

data Coord a = Coord a a deriving Show

getCenter :: Double -> Coord Int -> Coord Double
getCenter width (Coord x y) = (Coord (width * fromIntegral x + width / 2) (width * fromIntegral y + width / 2))

getCell :: Double -> Coord Double -> Coord Int
getCell width (Coord x y) = (Coord (floor $ x / width)  (floor $ y / width))