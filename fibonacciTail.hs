{- Функция для вычисления n-го числа Фибоначчи с помощью механизма аккумуляторов (хвостовая рекурсия),
   имеющая линейную сложность (по числу рекурсивных вызовов) и определенная для всех целых чисел.
-}

fibonacciTail :: Integer -> Integer
fibonacciTail n = helper 1 0 n

helper prev curr n | n == 0    = curr
                   | n > 0     = helper curr (prev + curr) (n - 1)
                   | otherwise = helper (curr - prev) prev (n + 1)