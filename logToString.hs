{- Функция logLevelToString возвращает текстуальное представление типа LogLevel.
   Функция logEntryToString возвращает текстуальное представление записи в виде: <время>: <уровень>: <сообщение>
-}

import Data.Time.Clock
import Data.Time.Format
import System.Locale

infixl 1 &
(&) :: a -> (a -> b) -> b
(&) x f = f x

timeToString :: UTCTime -> String
timeToString = formatTime defaultTimeLocale "%a %d %T"

data LogLevel = Error | Warning | Info

data LogEntry = LogEntry {timestamp :: UTCTime, logLevel :: LogLevel, message :: String}

logLevelToString :: LogLevel -> String
logLevelToString lvl = case lvl of
	Error   -> "Error"
	Warning -> "Warning"
	Info    -> "Info"

logEntryToString :: LogEntry -> String
logEntryToString entry = (timeToString $ entry & timestamp) ++ ": " ++ (logLevelToString $ entry & logLevel) ++ ": " ++ (entry & message)