{- Функция возвращает в обратном алфавитном порядке список символов,
   попадающих в заданный парой диапазон. Попадание символа x в диапазон пары (a,b) означает, что x >= a и x <= b.

   GHCi> revRange ('a','z')
   "zyxwvutsrqponmlkjihgfedcba"
-}

import Data.List (unfoldr)

revRange :: (Char, Char) -> [Char]
revRange (from, to) = unfoldr g to where
		g = (\x -> if x < from then Nothing else Just (x, pred x))