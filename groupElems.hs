{- Функция группирует одинаковые элементы в списке (если они идут подряд) и возвращает список таких групп.
-}

groupElems :: Eq a => [a] -> [[a]]
groupElems xs = helper xs [] where
	helper [] [] = []
	helper [] ys = [ys]
	helper (x:xs) [] = helper xs ([x])
	helper (x:xs) ys = if x == head ys then helper xs (x:ys) else ys : (helper xs [x])