{- Тип нечетных чисел, представитель класса типов Enum.
-}

data Odd = Odd Integer 
  deriving (Eq, Show)

instance Enum Odd where
	succ (Odd x) = Odd (x + 2)
	pred (Odd x) = Odd (x - 2)
	toEnum x = Odd (toInteger x)
	fromEnum (Odd x) = fromIntegral x
	enumFrom (Odd x) = (Odd x) : enumFrom (succ (Odd x))
	enumFromTo (Odd from) (Odd to) | from > to = []
								   | otherwise = takeWhile (\x -> x /= succ (Odd to)) (enumFrom (Odd from))
	enumFromThen (Odd from) (Odd next) = (Odd from) : enumFromThen (Odd next) (Odd (next + next - from))
	enumFromThenTo (Odd from) (Odd next) (Odd to) = take
													(1 + (fromIntegral((to - from) `div` (next - from))))
													(enumFromThen (Odd from) (Odd next))