{- Функция выбрасывает из списка элементы, стоящие на нечетных местах, оставляя только четные.
   Функция позволяет работать и с бесконечными списками.

   GHCi> evenOnly [1..10]
   [2,4,6,8,10]
   GHCi> evenOnly ['a'..'z']
   "bdfhjlnprtvxz"
   GHCi> take 3 (evenOnly [1..])
   [2,4,6]
-}

evenOnly :: [a] -> [a]
evenOnly xs = foldr (\(cnt, y) ys -> if even cnt then y:ys else ys) [] (zip [1..] xs)