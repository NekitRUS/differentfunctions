{- Представители класса Functor для типа данных Point3D и GeomPrimitive.
-}

data Point3D a = Point3D a a a deriving Show

data GeomPrimitive a = Point (Point3D a) | LineSegment (Point3D a) (Point3D a) deriving Show

instance Functor Point3D where
    fmap f (Point3D x y z) = Point3D (f x) (f y) (f z)

instance Functor GeomPrimitive where
    fmap f (Point pnt3D)               = Point (fmap f pnt3D)
    fmap f (LineSegment pnt3D1 pnt3D2) = LineSegment (fmap f pnt3D1) (fmap f pnt3D2)