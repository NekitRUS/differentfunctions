{- Функция height возвращает высоту бинарного дерева.Ф
   Функция size возвращает количество узлов в дереве (и внутренних, и листьев).
   Считается, что дерево, состоящее из одного листа, имеет высоту 0.
-}

import Control.Monad.State

data Tree a = Leaf a | Node (Tree a) (Tree a) deriving Show

height :: Tree a -> Int
height (Leaf _)          = 0
height (Node left right) = 1 + (max (height left) (height right))

size :: Tree a -> Int
size (Leaf _)          = 1
size (Node left right) = 1 + size left + size right