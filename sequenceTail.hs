{- Функция, находящая элементы следующей рекуррентной последовательности при помощи хвостовой рекурсии:
   a(0) = 1, a(1) = 2, a(2) = 3, a(k+3) = a(k+2) + a(k+1) - 2*a(k).
-}

seqA :: Integer -> Integer
seqA n 
    | n >= 0 = let
        helper pprev prev curr n
        	| n == 0    = 1
        	| n == 1    = 2
        	| n == 2    = curr
        	| otherwise = helper prev curr (curr + prev - 2 * pprev) (n - 1)
      in helper 1 2 3 n
    | otherwise = error "arg must be positive"