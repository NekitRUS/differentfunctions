{- Функция, находящая сумму и количество цифр десятичной записи заданного целого числа.
-}

sum'n'count :: Integer -> (Integer, Integer)
sum'n'count x
    | x == 0    = (0, 1)
    | x < 0     = helper 0 0 (-x)
    | otherwise = helper 0 0 x
  where helper sum count x 
              | x == 0    = (sum, count)
              | otherwise = helper (sum + x `mod` 10) (count + 1) (x `div` 10)