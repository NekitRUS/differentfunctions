{-  Функция для вычисления n-го числа Фибоначчи с возможностью работы на отрицательных индексах
-}

fibonacci :: Integer -> Integer
fibonacci n | n == 0                  = 0
            | (n == 1) || (n == (-1)) = 1
            | n > 1                   = fibonacci (n - 1) + fibonacci (n - 2)
            | otherwise               = (-1) ^ abs (n + 1) * (fibonacci (-n))