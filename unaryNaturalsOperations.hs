{- Можно считать, что элементы типа Nat - это натуральные числа в унарной системе счисления.
   Функция add складывает такие числа, mul - перемножает, fac - вычисляет факториал.
-}

data Nat = Zero | Suc Nat deriving Show

fromNat :: Nat -> Integer
fromNat Zero = 0
fromNat (Suc n) = fromNat n + 1

toNat :: Integer -> Nat
toNat 0 = Zero
toNat a = Suc (toNat (a - 1))

add :: Nat -> Nat -> Nat
add a b = toNat (fromNat a + fromNat b)

mul :: Nat -> Nat -> Nat
mul a b = toNat (fromNat a * fromNat b)

fac :: Nat -> Nat
fac n = toNat(helper (fromNat n) 1) where
	helper 0 acc = acc
	helper n acc = helper (n - 1) (acc * n)