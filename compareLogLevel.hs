{- Функция сравнивает элементы типа LogLevel так, чтобы было верно, что Error > Warning > Info.
-}

data LogLevel = Error | Warning | Info

cmp :: LogLevel -> LogLevel -> Ordering
cmp Error Error     = EQ
cmp Warning Warning = EQ
cmp Info Info       = EQ
cmp Error _         = GT
cmp _ Error         = LT
cmp Info _          = LT
cmp _ Info          = GT