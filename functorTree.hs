{- Представитель класса Functor для бинарного дерева, в каждом узле которого хранятся элементы типа Maybe.
-}

import Data.Functor ((<$>))

data Tree a = Leaf (Maybe a) | Branch (Tree a) (Maybe a) (Tree a) deriving Show

instance Functor Tree where
    fmap f (Leaf lf) = case lf of
    	Nothing -> Leaf Nothing
    	Just x  -> Leaf (Just $ f x)

    fmap f (Branch leftTree val rightTree) = case val of
    	Nothing -> Branch (f <$> leftTree) Nothing (f <$> rightTree)
    	Just x  -> Branch (f <$> leftTree) (Just $ f x) (f <$> rightTree)