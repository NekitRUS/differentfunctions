{- Лексер арифметических выражений.

   GHCi> tokenize "1 + ( 7 - 2 )"
   Just [Number 1,Plus,LeftBrace,Number 7,Minus,Number 2,RightBrace]

   GHCi> tokenize "1 + abc"
   Nothing
-}

import Data.Char (isDigit)

data Token = Number Int | Plus | Minus | LeftBrace | RightBrace     
     deriving (Eq, Show)

asToken :: String -> Maybe Token
asToken x | x == "+"      = Just Plus
		  | x == "-"      = Just Minus
		  | x == "("      = Just LeftBrace
		  | x == ")"      = Just RightBrace
		  | all isDigit x = Just (Number (read x :: Int))
		  | otherwise     = Nothing

tokenize :: String -> Maybe [Token]
tokenize input = mapM asToken $ words input