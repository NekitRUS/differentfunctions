{- Функция, «поднимающая» вычисление из монады Reader в монаду State.

   GHCi> evalState (readerToState $ asks (+2)) 4
   6
   
   GHCi> runState (readerToState $ asks (+2)) 4
   (6,4) 
-}

import Control.Monad.Reader
import Control.Monad.State

readerToState :: Reader r a -> State r a
readerToState m = state (\st -> (runReader m st, st))