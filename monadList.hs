{- Функция, спользуя монаду списка и do-нотацию, принимает на вход некоторое число x
   и возвращает список троек (a,b,c), таких что
   
   a^2 + b^2 = c^2, a > 0, b > 0, c > 0, c <= x, a < b.
   
   Число x может быть ≤ 0, на таком входе возвращается пустой список.
-}

pythagoreanTriple :: Int -> [(Int, Int, Int)]
pythagoreanTriple x | x <= 0    = []
					| otherwise = do
						c <- [1..x]
						b <- [1..x]
						a <- [1..(b - 1)]
						True <- return (a*a + b*b == c*c)
						return (a, b, c)