{- Представитель класса типов Monoid для Maybe' a с условием, что mempty не равен Maybe' Nothing. 
-}

import Data.Monoid

newtype Maybe' a = Maybe' { getMaybe :: Maybe a }
    deriving (Eq,Show)

instance Monoid a => Monoid (Maybe' a) where
    mempty = Maybe' (Just mempty)
    _ `mappend` Maybe' Nothing                = Maybe' Nothing
    Maybe' Nothing `mappend` _                = Maybe' Nothing
    Maybe' (Just l) `mappend` Maybe' (Just r) = Maybe' (Just (l `mappend` r))