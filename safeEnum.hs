{- Класс типов SafeEnum содержит две функции, которые ведут себя как `succ` и `pred` стандартного класса Enum, однако являются тотальными,
   то есть не останавливаются с ошибкой на наибольшем и наименьшем значениях типа-перечисления соответственно, а обеспечивают циклическое поведение.
-}

class (Eq a, Enum a, Bounded a) => SafeEnum a where
  ssucc :: a -> a
  ssucc x = if (x == maxBound) then minBound else succ x

  spred :: a -> a
  spred x = if (x == minBound) then maxBound else pred x