{- Функцию считает среднее арифметическое всех значений в дереве, осуществляя только один проход по дереву.
-}

data Tree a = Leaf a | Node (Tree a) (Tree a) deriving Show

avg :: Tree Int -> Int
avg t =
    let (cnt, sum) = go t
    in sum `div` cnt
  where
    go :: Tree Int -> (Int,Int)
    go (Leaf n) = (1, n)
    go (Node left right) = (leftCnt + rightCnt, leftSum + rightSum) where
    	(leftCnt, leftSum)   = go left
    	(rightCnt, rightSum) = go right