{- Функция, «поднимающая» вычисление из монады Writer в монаду State.

   GHCi> runState (writerToState $ tell "world") "hello,"
   ((),"hello,world")

   GHCi> runState (writerToState $ tell "world") mempty
   ((),"world")
-}

import Control.Monad.Writer
import Control.Monad.State

writerToState :: Monoid w => Writer w a -> State w a
writerToState m = state $(\st -> (res, st `mappend` out)) where
	(res, out) = runWriter m