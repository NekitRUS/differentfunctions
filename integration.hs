{- Функция, находящая значение определённого интеграла от заданной функции f
   на заданном интервале [a,b] методом трапеций.
-}

integration :: (Double -> Double) -> Double -> Double -> Double
integration f a b = h * (((f a + f b) / 2) + helper f (a + h) 0 n)
  where
    n = 1000
    h = (b - a) / n
    helper func start sum num
              | num == 1  = sum
              | otherwise = helper func (start + h) (sum + func start) (num - 1)