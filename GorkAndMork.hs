{- Пусть существуют два класса типов KnownToGork и KnownToMork,
   которые предоставляют методы stomp (stab) и doesEnrageGork (doesEnrageMork) соответственно.

   Класс типов KnownToGorkAndMork является расширением обоих этих классов, предоставляя дополнительно метод stompOrStab.

   Метод stompOrStab вызывает метод stomp, если переданное ему значение приводит в ярость Морка;
   вызывает stab, если оно приводит в ярость Горка и вызывает сначала stab, а потом stomp, если оно приводит в ярость их обоих.
   Если не происходит ничего из вышеперечисленного, метод возвращает переданный ему аргумент.
-}

class KnownToGork a where
    stomp :: a -> a
    doesEnrageGork :: a -> Bool

class KnownToMork a where
    stab :: a -> a
    doesEnrageMork :: a -> Bool

class (KnownToGork a, KnownToMork a) => KnownToGorkAndMork a where
    stompOrStab :: a -> a
    stompOrStab x | doesEnrageGork x && doesEnrageMork x = stomp (stab x)
    			  | doesEnrageMork x = stomp x
    			  | doesEnrageGork x = stab x
    			  | otherwise = x