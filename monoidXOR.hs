{- Представитель класса типов Monoid для типа Xor, в котором mappend выполняет операцию xor.
-}

import Data.Monoid

newtype Xor = Xor { getXor :: Bool }
    deriving (Eq,Show)

instance Monoid Xor where
    mempty = Xor False
    (Xor a) `mappend` (Xor b) = Xor (not (a && b) && (a || b))