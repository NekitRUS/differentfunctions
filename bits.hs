{- Функция сложения и умножения для целых чисел, представленных как список битов со знаком
   (младшие биты идут в начале списка, а старшие — в конце).
-}

data Bit = Zero | One deriving (Show, Read)
data Sign = Minus | Plus deriving (Show, Read)
data Z = Z Sign [Bit] deriving (Show, Read)

bitsToInt :: [Bit] -> [Int]
bitsToInt [] = []
bitsToInt (x:xs) = case x of Zero -> 0:(bitsToInt xs); One -> 1:(bitsToInt xs)

convertToInt :: Z -> Int
convertToInt (Z Minus bits) = -(convertToInt (Z Plus bits))
convertToInt (Z Plus bits)  = foldr (\(pos, bit) ys -> 2 ^ pos * bit + ys) 0 (zip [0..] (bitsToInt bits))

intToBits :: Int -> [Bit]
intToBits 0 = []
intToBits x = (if x `mod` 2 == 0 then Zero else One):(intToBits (x `div` 2))

convertToBits :: Int -> Z
convertToBits x = if x < 0 then (Z Minus $ intToBits (-x)) else (Z Plus $ intToBits x)

add :: Z -> Z -> Z
add a b = convertToBits $ convertToInt a + convertToInt b

mul :: Z -> Z -> Z
mul a b = convertToBits $ convertToInt a * convertToInt b