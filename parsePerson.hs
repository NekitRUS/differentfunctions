{- Функция parsePerson разбирает строки вида

   firstName = John\nlastName = Connor\nage = 30

   и возвращает либо результат типа Person, либо ошибку типа Error.

   Строка, которая подается на вход, должна разбивать по символу '\n' на список строк, каждая из которых имеет вид X = Y.
   Если входная строка не имеет указанный вид, то функция возвращает ParsingError.
   Если указаны не все поля, то возвращается IncompleteDataError.
   Если в поле age указано не число, то возвращается IncorrectDataError str, где str — содержимое поля age.
   Если в строке присутствуют лишние поля, то они игнорируются.
-}

import Data.List.Split (splitOn)
import Data.Char (isDigit)

data Error = ParsingError | IncompleteDataError | IncorrectDataError String deriving Show

data Person = Person { firstName :: String, lastName :: String, age :: Int } deriving Show

parsePerson :: String -> Either Error Person
parsePerson str = let
		processing = if all isValid rawData then isComplete else Left ParsingError
		rawData = map (splitOn " = ") (splitOn "\n" str)

		isValid [(_:_), (_:_)] = True
		isValid _              = False

		isComplete = case (getValue "firstName", getValue "lastName", getValue "age") of
			(Just fn, Just ln, Just age) -> createPerson fn ln age
			_                            -> Left IncompleteDataError

		getValue key = lookup key (map (\[k, v] -> (k, v)) rawData)

		createPerson fn ln age = if wrongAge age then Left (IncorrectDataError age) else Right (Person fn ln (read age :: Int)) where
				wrongAge []     = False
				wrongAge (x:xs) = if isDigit x then wrongAge xs else True

	in processing